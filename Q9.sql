
select 
-- どのカラムのものを？
item_category.category_name,
-- カテゴリー別で合計金額表示してtotal_priceとして出力するようにする
-- これが集約関数

item_category.category_name,
sum(item.item_price ) as total_price
from -- どこから
item
inner join 
item_category
on
item.category_id = item_category.category_id
group by 
item_category.category_name
order by
total_price desc
;


